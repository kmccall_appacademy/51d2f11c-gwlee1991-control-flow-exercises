# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  uppercase_str = ''

  i = 0
  while i < str.length
    if str[i] == str[i].upcase()
      uppercase_str += str[i]
    end
    i += 1
  end

  return uppercase_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 == 0
    return str[(str.length/2-1), 2]
  else
    return str[str.length/2.floor]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel = ['a', 'e', 'i', 'o', 'u']
  vowel_count = 0

  str.each_char do |char|
    if vowel.include?(char.downcase)
      vowel_count += 1
    end
  end

  return vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1

  (1..num).each do |n|
    product *= n
  end
  return product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joint = ''

  i = 0
  while i < arr.length - 1
    joint = joint + arr[i] + separator
    i = i + 1
  end

  return joint + arr[-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcase_str = ''
  chars = str.chars
  chars.each_index do |idx|
    if idx.even?
      weirdcase_str += str[idx].upcase
    else
      weirdcase_str += str[idx].downcase
    end
  end

  weirdcase_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  reversed_str = []
  words.each do |word|
    if word.length > 4
      reversed_str.push(word.reverse)
    else
      reversed_str.push(word)
    end
  end

  reversed_str.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      array.push('fizzbuzz')
    elsif num% 3 == 0
      array.push('fizz')
    elsif num % 5 == 0
      array.push('buzz')
    else
      array.push(num)
    end
  end

  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []

  arr.each do |ele|
    reversed.unshift(ele)
  end

  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num < 2
    return false
  end

  (2..num).each do |n|
    if num % n == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []

  (1..num).each do |n|
    if num % n == 0
      result.push(n)
    end
  end

  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_array = []
  factors = factors(num)
  factors.each do |factor|
    if prime?(factor)
      prime_array.push(factor)
    end
  end

  prime_array
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = arr.select {|i| i.odd?}
  even = arr.select {|i| i.even?}

  if odd.length > even.length
    even
  else
    odd
  end
end
